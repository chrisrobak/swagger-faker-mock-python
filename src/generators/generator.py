"""
Base generator class for common actions across doc types.
"""
import os
from jinja2 import Environment, FileSystemLoader

class Generator(object):
    current_path = os.path.split(os.path.split(__file__)[0])[0]
    JINJA_ENV = Environment(
        loader = FileSystemLoader(
            os.path.join(current_path, 'templates')
        ),
        trim_blocks=True
    )
   
    def generate_server(self, config):
        server_template = Generator.JINJA_ENV.get_template('server.j2')
        return server_template.render(**config)

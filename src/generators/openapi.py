from .generator import Generator

class OpenAPI(Generator):
    def __init__(self, api_spec):
        self.api_spec = api_spec

    def generate(self, port):
        if not port:
            port = 5000
        config = {
            'app_settings': {
                'debug': True,
                'port': port
            },
            'routes': self.__generate_routes()
        }
        server = self.generate_server(config)
        return server

    def __generate_routes(self):
        route_template = self.JINJA_ENV.get_template('route.j2')
        routes = []
        for path, path_config in self.api_spec['paths'].items():
            resource_name = path.split('/')[1]
            if path.split('/')[2] != "":
                resource_name += "byId"
            routes.append(
                route_template.render(
                    resource_name=resource_name,
                    path=path,
                    path_config=path_config,
                    schemas=self.api_spec['components']['schemas']
               )
            )
        return routes

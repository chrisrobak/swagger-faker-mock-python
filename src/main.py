"""
Generates a mock server serving fake data
from a swagger document.

Usage:
    main.py <path_to_api_doc> [options]

Options:
    -h --help                  Shows this screen
    --port=<port>              The port the mock server will run on
    --out=<out>                The place to output the server
"""
from docopt import docopt
import json
import os
from generators.openapi import OpenAPI

class FileNotFound(Exception):
    """
    Raise when file does not exist.
    """

class InvalidSpecification(Exception):
    """
    Raise when spec doc is invalid
    """    


def main(args):
    api_spec = get_json_file(args['<path_to_api_doc>'])
    if 'openapi' in api_spec:
        print('Working with OpenAPI spec version: {}'.format(api_spec['openapi']))
        generator = OpenAPI(api_spec)
    elif 'swagger' in api_spec:
        print('Working with Swagger spec version: {}'.format(api_spec['swagger']))
    else:
        raise InvalidSpecification('Uknown/Unsupported document version')

    server = generator.generate(args['--port'])
    with open('mock_server.py', 'w') as outfile:
        outfile.writelines(server)
    print(server)

def get_json_file(file_path):
    if not os.path.isfile(file_path):
        raise FileNotFound("{} is not a file!".format(file_path))
    with open(file_path, 'r') as spec_doc:
        return json.load(spec_doc)

    

if __name__ == "__main__":
    args = docopt(__doc__)
    print(args)
    main(args)

